#pragma once

#define DEFAULT_TEMPO 120
#define QTY_STEPS 16
#define QTY_TRACKS 7
#define QTY_UNITS 6

#include "ofMain.h"
#include "Mode.h"
#include "ofxDatGui.h"
#include "Sounds.h"
#include "ofxSimpleTimer.h"
#include "Utils.h"
#include "ofxJSON.h"

class SequenceModeTrack {
    public:
        static Sounds * sounds;

        SequenceModeTrack();

        void toggleSound( Sound * sound );

        void processSequence();
        void processSequenceOffset();

        void draw( bool notSelected = false );
        void onTempo();

        void unselectAllSounds();
        void selectAllSounds();

        void setCantSteps(int value);
        void setOffset(int value);
        void setProbability(float value);

        bool playing = true;
        int cantSteps = QTY_STEPS;

        int lastStep = -1;
        int currentStep = -1;

        int offset = 0;
        float probability = 1;

        vector<Sound*> secuencia;
    private:
        Sound ** secuenciaEnTiempo = NULL;

        ofPolyline polyLine;
        bool isProcessing = false;

};

class SequenceMode : virtual public Mode {
private:
    ofxDatGui *gui;
    Sounds *sounds;

    SequenceModeTrack tracks[QTY_TRACKS];
    SequenceModeTrack * selectedTrack;
    // vector<Sound*> secuencia;
    // Sound * secuenciaEnTiempo[QTY_STEPS];

    ofxDatGuiSlider *guiBPM = NULL;
    ofxDatGuiToggle *guiUseMidiClock = NULL;
    ofxDatGuiToggle *guiTempo = NULL;
    ofxDatGuiToggle *playOnHover = NULL;
    ofxDatGuiMatrix *guiTracks = NULL;
    ofxDatGuiSlider *guiOffset = NULL;
    ofxDatGuiSlider *guiProbability = NULL;
    ofxDatGuiBreak *guiBreak1 = NULL;
    ofxDatGuiBreak *guiBreak2 = NULL;

    ofxDatGuiToggle *guiPlayPause = NULL;
    ofxDatGuiDropdown *guiUnit = NULL;
    string strUnits[QTY_UNITS] = {"1 bar", "2 bars", "3 bars", "4 bars", "1/2 bar", "1/4 bar"};
    int intUnits[QTY_UNITS] = {QTY_STEPS, QTY_STEPS*2, QTY_STEPS*3, QTY_STEPS*4, QTY_STEPS /2, QTY_STEPS/4};

    // ofxDatGuiButton *btnAddTrack = NULL;
    // ofxDatGuiButton *btnRemoveTrack = NULL;

    ofxSimpleTimer tempo;

    void onTempo(int &args);

public:
    SequenceMode(Sounds *_sounds, ofxDatGui *_gui);

    static int currentBeat;

    void setupGui();
    Json::Value save();
    void load( Json::Value jsonData );

    void mousePressed(ofVec2f p, int button);
    void mouseDragged(ofVec2f p, int button);
    void midiMessage(Utils::midiMsg m);

    void onBPMChange(ofxDatGuiSliderEvent e);
    void onOffsetChange(ofxDatGuiSliderEvent e);
    void onProbabilityChange(ofxDatGuiSliderEvent e);
    void onMatrixEvent(ofxDatGuiMatrixEvent e);
    void onPlayPause(ofxDatGuiToggleEvent e);
    void onUseMidiClock(ofxDatGuiToggleEvent e);
    void onChosenUnit(ofxDatGuiDropdownEvent e);
    // void onAddRemoveTrackEvent(ofxDatGuiButtonEvent e);

    void update();
    void beforeDraw();
    void draw();

    void processSequence();

    void onSelectedMode();
    void onUnselectedMode();

    static bool isModeActive;

    // void onPlayOnHoverEvent(ofxDatGuiToggleEvent e);
};
