#ifndef _PARTICLE_MODELS
#define _PARTICLE_MODELS

#include "ofMain.h"
#include "Particle.h"

class SimpleParticle: public Particle {
    public:
        ofVec2f velocity;
        ofVec2f acceleration;

        SimpleParticle();
        void customUpdate(map<string,ofPoint> params);
    
};

class SpreadedParticle: public Particle {
    public:
        ofVec2f velocity;
        ofVec2f acceleration;
        ofVec2f spreadAmt;

        SpreadedParticle();
        void customUpdate(map<string,ofPoint> params);


};

class RadialParticle: public Particle {
    public:
        ofVec2f velocity;
        ofVec2f acceleration;

        RadialParticle();
        void customUpdate(map<string,ofPoint> params);
        
        //void customUpdate();
};
#endif

