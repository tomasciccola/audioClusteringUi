#include "ParticleModels.h"

//Simple Model
SimpleParticle::SimpleParticle() {
    velocity = ofVec2f(0.0,0.0);
    acceleration = ofVec2f(0.0,0.0);
}
void SimpleParticle::customUpdate(map<string,ofPoint> params) {
    acceleration = params["Acceleration"];
    acceleration.x = ofMap(acceleration.x, 0,1,-0.2,0.2);
    acceleration.y = ofMap(acceleration.y, 0,1,-0.2,0.2);
    velocity += acceleration;
    position += velocity;
}

//Spreaded Model
SpreadedParticle::SpreadedParticle() {
    velocity = ofVec2f(0.0,0.0);
    acceleration = ofVec2f(0.0,0.0);
    spreadAmt = ofVec2f(0.0,0.0);
}

void SpreadedParticle::customUpdate(map<string, ofPoint> params) {
    acceleration = params["Acceleration"];
    acceleration.x = ofMap(acceleration.x, 0,1,-1.0,1.0);
    acceleration.y = ofMap(acceleration.y, 0,1,-1.0,1.0);

    spreadAmt = params["Spread Amount"];

    //velocity += ofRandom(-1.0, 1.0) * spreadAmt;
    velocity.set(ofRandom(-1, 1) * spreadAmt.x,
            ofRandom(-1, 1) * spreadAmt.y);
    velocity += acceleration;
    position += velocity;
}
//RadialModel
RadialParticle::RadialParticle(){
    velocity.x = ofRandom(-1,1);
    velocity.y = ofRandom(-1,1);
    acceleration.x = velocity.x * 0.3;
    acceleration.y = velocity.y * 0.3;
}
void RadialParticle::customUpdate(map<string,ofPoint> params){
   velocity += acceleration;
   position += velocity;
}
