#include "ParticleMode.h"
/*
##################################################################
                       PARTICLE MODE
##################################################################
*/
ParticleMode::ParticleMode(Sounds *_sounds, ofxDatGui *_gui) {
    sounds = _sounds;
    gui = _gui;

    modeName = "Particle Mode";
    iconPath = "assets/icons/particle_mode.png";
    iconPathActive = "assets/icons/particle_mode_active.png";

    drawer = new ParticleRegionsManager();

    modelNames.push_back("Simple");
    modelNames.push_back("Spreaded");
    modelNames.push_back("Radial");
    atModel = 0;

    particlesAge = 5.0;
    randomizeEmitter = false;

}
ParticleMode::ParticleMode(Sounds *_sounds) {
    sounds = _sounds;
}

vector<Particle*> ParticleMode::add(ofVec2f p){
    vector<Particle*> ps;
    if(modelNames[atModel] == "Simple") {
        ps.push_back(new SimpleParticle());
        ps[0]->setPosition(p);
        particles.push_back(ps[0]);
    } else if(modelNames[atModel] == "Spreaded") {
        ps.push_back(new SpreadedParticle());
        ps[0]->setPosition(p);
        particles.push_back(ps[0]);
    } else if(modelNames[atModel] == "Radial") {
        for(int i = 0; i< 20; i++) {
            ps.push_back(new RadialParticle());
            ps[i]->setPosition(p);
            particles.push_back(ps[i]);
        }
    }
    return ps;
}
void ParticleMode::update() {
    for (int i = 0; i < particles.size(); i++) {

        sounds->playSoundAt(particles[i]->getPosition());
        particles[i]->age = particlesAge;
        map<string,ofPoint> pVals = modelParams[atModel]->getParamsVal();
        particles[i]->update(pVals);

        if (particles[i]->isDead()) {
            delete particles[i];
            particles.erase(particles.begin() + i);
        }
    }
    drawer->update();
}
void ParticleMode::draw() {
    for (int i = 0; i < particles.size(); i++) {
        particles[i]->draw();
    }
}

void ParticleMode::beforeDraw(){
    drawer->draw();
}

void ParticleMode::setupGui() {

    params.ageSlider = gui->addSlider("Age", 0.0, 10.0);
    params.ageSlider->setValue(particlesAge);
    params.ageSlider->onSliderEvent(this, &ParticleMode::onAgeChanged);

    params.randomizeEmitter = gui->addToggle("Randomize Emitter");
    params.randomizeEmitter->setChecked(randomizeEmitter);
    params.randomizeEmitter->onToggleEvent(this, &ParticleMode::onRandomizeEmitterSetted);

    params.modelSelector = gui->addDropdown("Models", modelNames);
    params.modelSelector->onDropdownEvent(this, &ParticleMode::onSelectedModel);
    params.modelSelector->select(atModel);




    guiComponents.push_back(params.randomizeEmitter);
    guiComponents.push_back(params.ageSlider);
    guiComponents.push_back(params.modelSelector);

    setModelsGui();

    for (int i = 0; i<modelParams.size(); i++) {
        vector<ofxDatGui2dPad*> modelParamComponents =
            modelParams[i]->getComponents();

        for(int j=0; j<modelParamComponents.size(); j++) {
            guiComponents.push_back(modelParamComponents[j]);
        }
    }

}
void ParticleMode::setModelsGui() {
    setSimpleModelGui();
    setSpreadedModelGui();
    setRadialModelGui();
}
void ParticleMode::setSimpleModelGui() {
    ParticleModelParams *simpleModelParams = new ParticleModelParams(gui);
    simpleModelParams->addParam("Acceleration", ofRectangle(0.0,0.0,1.0,1.0));
    modelParams.push_back(simpleModelParams);


}
void ParticleMode::setSpreadedModelGui() {
    ParticleModelParams *spreadedModelParams = new ParticleModelParams(gui);
    spreadedModelParams->addParam("Acceleration", ofRectangle(0.0,0.0,1.0,1.0));
    spreadedModelParams->addParam("Spread Amount", ofRectangle(0.0,0.0,8.0,8.0));
    modelParams.push_back(spreadedModelParams);

}

void ParticleMode::setRadialModelGui() {
    ParticleModelParams *radialModelParams = new ParticleModelParams(gui);
    radialModelParams->addParam("Acceleration", ofRectangle(0.0,0.0,1.0,1.0));
    modelParams.push_back(radialModelParams);
}

void ParticleMode::onAgeChanged(ofxDatGuiSliderEvent e){

   particlesAge = e.target->getValue();

}

void ParticleMode::onRandomizeEmitterSetted(ofxDatGuiToggleEvent e){
    randomizeEmitter = !randomizeEmitter;
}
void ParticleMode::onSelectedModel(ofxDatGuiDropdownEvent e) {
    modelParams[atModel]->setGuiVisible(false);
    modelParams[e.child]->setGuiVisible(true);
    atModel = e.child;
}
void ParticleMode::mousePressed(ofVec2f p, int button) {

    if(MidiServer::midiLearn) {

    } else {
        if(button == 0) {//boton izquierdo
            add(p);
        }
    }
}
void ParticleMode::mouseDragged(ofVec2f p, int button) {

    if(MidiServer::midiLearn) {
        drawer->mouseDragged(p);
    }
}
void ParticleMode::mouseReleased(ofVec2f p) {
    if(MidiServer::midiLearn) {
        drawer->mouseReleased();
    }
}
void ParticleMode::keyPressed(int key) {
}
void ParticleMode::midiMessage(Utils::midiMsg m) {

    if(m.status == "Note On") {
        drawer->onNoteOn(m.pitch);
        if(drawer->status == "idle"){
            for(int i = 0;i< drawer->areas.size();i++){
                if(drawer->areas[i]->note == m.pitch){
                  vector<Particle*> ps;

                  if(randomizeEmitter){
                    ps = add(drawer->areas[i]->getRandomPointInside());
                  }else{
                    ps = add(drawer->areas[i]->centerPoint);
                  }

                   for(int j = 0; j< ps.size(); j++){
                     ps[j]->setArea(drawer->areas[i]);
                   }
                   MidiServer::midiLearn = false;
                   //btnMidiLearn->setChecked(false);
                }
            }
        }
    }else if(m.status == "Control Change"){
        if(MidiServer::midiLearn){


            //btnMidiLearn->setChecked(false);
        }
    }
}
void ParticleMode::onSelectedMode() {
    //invisibiliza y visibilizá según corresponda
    for(int i = 0; i<modelParams.size(); i++) {
        if(i != atModel) {
            modelParams[i]->setGuiVisible(false);
        } else {
            modelParams[i]->setGuiVisible(true);
        }
    }
}
Json::Value ParticleMode::save(){
  Json::Value root = Json::Value(Json::objectValue);
  Json::Value midiMappings = Json::Value(Json::arrayValue);

  for(u_int i = 0; i< drawer->areas.size(); i++){
    ParticleRegion* area = drawer->areas[i];
    Json::Value midiMapping = Json::Value(Json::objectValue);
    midiMapping["Note"] = area->note;
    midiMapping["CenterX"] = area->centerPoint.x;
    midiMapping["CenterY"] = area->centerPoint.y;
    midiMapping["Radius"] = abs(area->radius);
    midiMappings[i] = midiMapping;
  }
  root["midiMappings"] =  midiMappings;
  return root;

}

void ParticleMode::load(Json::Value jsonData){
  Json::Value midiMappings = jsonData["midiMappings"];
  if(midiMappings != Json::nullValue){
    for(u_int i = 0 ; i < midiMappings.size(); i++){
      Json::Value midiMapping = midiMappings[i];
      ParticleRegion* area = new ParticleRegion();

      area->assigned = true;
      area->note = midiMapping["Note"].asInt();
      area->centerPoint.x = midiMapping["CenterX"].asFloat();
      area->centerPoint.y = midiMapping["CenterY"].asFloat();
      area->radius = midiMapping["Radius"].asFloat();

      drawer->areas.push_back(area);
    }
  }

}
