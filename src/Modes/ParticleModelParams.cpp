#include "ParticleModelParams.h"

ParticleModelParams::ParticleModelParams(ofxDatGui *_gui) {
    gui = _gui;
}

void ParticleModelParams::addParam(string label, ofRectangle r) {
    ofxDatGui2dPad *param = gui->add2dPad(label, r);
    param->on2dPadEvent(this,&ParticleModelParams::on2dPadEvent);
    params.push_back(param);
}
map<string, ofPoint> ParticleModelParams::getParamsVal() {
    map<string,ofPoint> map;
    for(int i = 0; i< params.size(); i++) {
        map[params[i]->getLabel()] = params[i]->getPoint();
    }
    return map;
}

void ParticleModelParams::on2dPadEvent(ofxDatGui2dPadEvent e) {
    //ofLogNotice(ofToString(e.x));
}

void ParticleModelParams::setGuiVisible(bool v) {
    for (int i = 0 ; i < params.size() ; i++) {
        params[i]->setVisible(v);
        params[i]->setEnabled(v);
    }
}

vector<ofxDatGui2dPad*> ParticleModelParams::getComponents() {
    return params;
}
