#pragma once

#include "ofMain.h"
#include "Mode.h"
#include "ofxDatGui.h"
#include "Sounds.h"
#include "MidiServer.h"

class ExplorerMode: virtual public Mode {
private:
    ofxDatGui *gui;
    Sounds *sounds;




    //nota midi -> idSonido
    std::unordered_map<int, int> midiMappings;
    Sound * midiTriggeredSound = nullptr;

public:
    ExplorerMode(Sounds *_sounds, ofxDatGui *_gui);
    void setupGui();

    void update();
    void mousePressed(ofVec2f p, int button);
    void midiMessage(Utils::midiMsg m);

    Json::Value save();
    void load( Json::Value jsonData );
};
