#pragma once

#include "ofMain.h"
#include "Mode.h"
#include "ofxDatGui.h"
#include "Sounds.h"
#include "AreaDrawer.h"
#include "ParticleRegions.h"
#include "Particles/Particle.h"
#include "Particles/ParticleModels.h"
#include "ParticleModelParams.h"
#include "ofxJSON.h"
#include "MidiServer.h"


class ParticleMode: virtual public Mode {
private:
    vector<Particle*> particles;
    vector<string> modelNames;
    int atModel;

    Sounds *sounds = NULL;
    ofxDatGui *gui = NULL;

    //Dibujo de áreas
    ParticleRegionsManager *drawer = NULL;

public:
    ParticleMode(Sounds *_sounds, ofxDatGui *_gui);
    ParticleMode(Sounds *_sounds);

    vector<Particle*> add(ofVec2f p);
    void update();
    void draw();
    void beforeDraw();

    void onSelectedMode();
    void onAgeChanged(ofxDatGuiSliderEvent e);
    void onRandomizeEmitterSetted(ofxDatGuiToggleEvent e);
    void onSelectedModel(ofxDatGuiDropdownEvent e);
    void setupGui();
    void setModelsGui();

    //modesGui
    void setSimpleModelGui();
    void setSpreadedModelGui();
    void setRadialModelGui();

    void mousePressed(ofVec2f p, int button);
    void keyPressed(int key);
    void mouseDragged(ofVec2f p, int button);
    void mouseReleased(ofVec2f p);
    void midiMessage(Utils::midiMsg m);
    bool midiLearn = false;

    //Sesión
    Json::Value save();
    void load( Json::Value jsonData );

    struct guiParams{
        ofxDatGuiSlider *ageSlider = NULL;
        ofxDatGuiDropdown *modelSelector = NULL;
        ofxDatGuiToggle *randomizeEmitter = NULL;
    }params;

    //SEPARAR EL ELEMENTO DE LA GUI DE LA VARIABLE QUE SETEA
    float particlesAge;
    bool randomizeEmitter;

    //Model params
    vector<ParticleModelParams*> modelParams;


};
