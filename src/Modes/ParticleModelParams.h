#ifndef _MODELPARAMS
#define _MODELPARAMS

#include "ofMain.h"
#include "ofxDatGui.h"

class ParticleModelParams {
public:
    ParticleModelParams(ofxDatGui *_gui);

    ofxDatGui *gui = nullptr;
    vector<ofxDatGui2dPad*> params;

    void addParam(string label, ofRectangle r);
    map<string,ofPoint> getParamsVal(); 
    vector<ofxDatGui2dPad*> getComponents();
    void setGuiVisible(bool v);
    void on2dPadEvent(ofxDatGui2dPadEvent e);

};
#endif
