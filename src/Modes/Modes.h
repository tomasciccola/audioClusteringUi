#pragma once

#include "ofMain.h"
#include "ofxDatGui.h"
#include "Sounds.h"
#include "ParticleMode.h"
#include "ExplorerMode.h"
#include "SequenceMode.h"
#include "Mode.h"
#include "Utils.h"
#include "ImageButton.h"
#include "MidiServer.h"
#include "ColorPaletteGenerator.h"

class Modes {
private:
    Mode *atMode;

    ofxDatGui *gui;
    Sounds *sounds;
//    ofxDatGuiDropdown *modeSelector;

public:
    vector<Mode*> modes;
    vector<string> modeNames;

    vector<ImageButton> modesButtons;

    Modes(Sounds *_sounds, ofxDatGui *_gui);
    void initModes();
    void setupGui();
//    void onGuiEvent(ofxDatGuiDropdownEvent e);
    void beforeDraw();
    void draw();
    void update();
    void mousePressed(int x, int y, int button);
    void modeSelected(int index);
    void keyPressed(int key);
    void mouseDragged(int x, int y, int button);
    void mouseReleased(int x, int y , int button);
    void mouseMoved(int x, int y);
    void midiMessage(Utils::midiMsg m);

};
