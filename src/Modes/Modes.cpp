#include "Modes.h"


Modes::Modes(Sounds *_sounds, ofxDatGui *_gui) {
    gui = _gui;
    sounds = _sounds;
    initModes();
}

void Modes::beforeDraw() {
    for(unsigned int i = 0; i < modes.size(); i++) {
        modes[i]->beforeDraw();
    }
}
void Modes::draw() {
    for(unsigned int i = 0; i < modes.size(); i++) {
        modes[i]->draw();
    }
}

void Modes::update() {
    for(unsigned int i = 0; i < modes.size(); i++) {
        modes[i]->update();
    }
}

void Modes::setupGui() {

    float currentX = ofGetWidth()/2 - modes.size()*ImageButton::buttonSize/2 ;
    float stepX = ImageButton::buttonSize;
    for ( auto * mode : modes ) {
        ImageButton imgButton(currentX,0,mode->iconPath, mode->iconPathActive);
        modesButtons.push_back( imgButton );
        currentX += stepX;
    }
    modesButtons[0].setActive(true);
}

void Modes::initModes() {
    modes.push_back(new ExplorerMode(sounds,gui));
    modes.push_back(new ParticleMode(sounds,gui));
    modes.push_back(new SequenceMode(sounds,gui));


    for(unsigned int i = 0; i < modes.size(); i++) {
        modeNames.push_back(modes[i]->getModeName());
    }

    setupGui();

    for(unsigned int i = 0; i < modes.size(); i++) {
        modes[i]->setupGui();
        if (i != 0 ) {
            modes[i]->setGuiVisible(false);
        }

        for (int j = 0 ; j < modes[i]->guiComponents.size() ; j++) {
            modes[i]->guiComponents[j]->setStripeColor(ColorPaletteGenerator::stripeMainColor);
        }
    }

    //selecciona un modo incial
    atMode = modes[0];
    atMode->setGuiVisible(true);
}

void Modes::mousePressed(int x, int y, int button) {
    atMode->mousePressed( ofVec2f(x,y), button );
}

void Modes::modeSelected(int index)
{
    for ( auto &button : modesButtons ) {
        button.setActive(false);
    }
    modesButtons[index].setActive(true);

    atMode->setGuiVisible(false);//oculta el de ahora
    atMode->onUnselectedMode();
    atMode = modes[index];
    atMode->setGuiVisible(true);//mostra el activo

    atMode->onSelectedMode();
}

void Modes::keyPressed(int key) {
    atMode->keyPressed(key);
}

void Modes::mouseDragged(int x, int y, int button){
    atMode->mouseDragged(ofVec2f(x,y), button);
}

void Modes::mouseReleased(int x, int y , int button) {
    atMode->mouseReleased(ofVec2f(x,y));
}

void Modes::mouseMoved(int x, int y) {
    atMode->mouseMoved(ofVec2f(x,y));
}
void Modes::midiMessage(Utils::midiMsg m) {


    //Si está en midiLearn sólo le manda mensaje al modo activo
    if(MidiServer::midiLearn){
        atMode->midiMessage(m);
    }else{
        for(unsigned int i = 0; i < modes.size(); i++) {
                modes[i]->midiMessage(m);
        }
    }
}
