#include "ExplorerMode.h"

ExplorerMode::ExplorerMode(Sounds *_sounds, ofxDatGui *_gui) {
    gui = _gui;
    sounds = _sounds;
    sounds->hoveredActivated = true;
    modeName = "Explorer Mode";

    iconPath = "assets/icons/explorer_mode.png";
    iconPathActive = "assets/icons/explorer_mode_active.png";
    // setupGui();
}
void ExplorerMode::setupGui() {
}

void ExplorerMode::update()
{
    //esto es así porque no se puede reproducir un Sound * desde midiMessage (threads capaz)
    if ( midiTriggeredSound ) {
        sounds->playSound(midiTriggeredSound);
        midiTriggeredSound = nullptr;
    }
}

void ExplorerMode::mousePressed(ofVec2f p, int button) {
    Sound * hoveredSound = sounds->getHoveredSound();
    if ( hoveredSound != NULL ) {
        sounds->playSound( hoveredSound );
    }
}

void ExplorerMode::midiMessage(Utils::midiMsg m)
{

    if ( m.status == "Note On" ) {

        if ( MidiServer::midiLearn && sounds->lastPlayedSound != nullptr ) {
            midiMappings[ m.pitch ] = sounds->lastPlayedSound->id;
            MidiServer::midiLearn = false;
        }

        if ( midiMappings.find(m.pitch) != midiMappings.end() ) {
            midiTriggeredSound = sounds->getSoundById( midiMappings[ m.pitch ] );
        }
    }
}

Json::Value ExplorerMode::save()
{
    Json::Value root = Json::Value( Json::objectValue );
    Json::Value jsonMidiMappings = Json::Value( Json::objectValue );

    for ( auto midiMapping : midiMappings) {
        jsonMidiMappings[ ofToString(midiMapping.first) ] = midiMapping.second;
    }

    root["midiMappings"] = jsonMidiMappings;
    return root;
}

void ExplorerMode::load(Json::Value jsonData)
{
    Json::Value jsonMidiMappings = jsonData["midiMappings"];
    if ( jsonMidiMappings != Json::nullValue ) {
        for( Json::Value::iterator itr = jsonMidiMappings.begin() ; itr != jsonMidiMappings.end() ; itr++ ) {
            ofLog() << itr.key() << ":" << *itr;
            midiMappings[ ofToInt( itr.key().asString() ) ] = (*itr).asInt();
        }
    }

}
