#ifndef _MODE
#define _MODE

#include "ofMain.h"
#include "ofxDatGui.h"
#include "Utils.h"
#include "ofxJSON.h"

class Mode {
private:

public:
    // ofxDatGuiFolder *panel = NULL;
    vector<ofxDatGuiComponent*> guiComponents;
    string modeName = "";

    string iconPath = "";
    string iconPathActive = "";

    void setGuiVisible(bool v) {
        for (int i = 0 ; i < guiComponents.size() ; i++) {
            guiComponents[i]->setVisible(v);
            guiComponents[i]->setEnabled(v);
        }
    }

    //Mode(){};
    virtual void beforeDraw() {}
    virtual void draw() {}
    virtual void update() {}

    virtual string getModeName() {
        return modeName;
    };

    virtual void mousePressed(ofVec2f p, int button) {};
    virtual void mouseDragged(ofVec2f p, int button){};
    virtual void mouseReleased(ofVec2f p){};
    virtual void keyPressed(int key) {};

    virtual void onSelectedMode(){};
    virtual void onUnselectedMode(){};

    virtual void mouseMoved(ofVec2f p){};
    virtual void midiMessage(Utils::midiMsg m){};

    virtual void setupGui(){};

    virtual Json::Value save(){ return Json::nullValue; };
    virtual void load( Json::Value jsonData ){};

    ~Mode() {};

};
#endif
