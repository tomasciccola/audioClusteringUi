#include "SequenceMode.h"

int SequenceMode::currentBeat;
bool SequenceMode::isModeActive = false;

SequenceMode::SequenceMode(Sounds *_sounds, ofxDatGui *_gui) {
    gui = _gui;

    sounds = _sounds;
    sounds->hoveredActivated = true;

    modeName = "Sequence Mode";
    iconPath = "assets/icons/sequence_mode.png";
    iconPathActive = "assets/icons/sequence_mode_active.png";

    SequenceModeTrack::sounds = _sounds;

    currentBeat = -1;
    int milis = (( 60.0 / DEFAULT_TEMPO ) * 1000);
    tempo.setup(milis/4); //semicorcheas
    tempo.start(true);

    ofAddListener( tempo.TIMER_COMPLETE, this, &SequenceMode::onTempo ) ;
}
void SequenceMode::setupGui() {
    // playOnHover = gui->addToggle("Play on hover");
    // playOnHover->setChecked(false);

    guiTracks = gui->addMatrix("Tracks", QTY_TRACKS ,true);
    guiTracks->onMatrixEvent(this,&SequenceMode::onMatrixEvent);
    guiTracks->setRadioMode(true);
    guiTracks->getChildAt(0)->setSelected(true);
    selectedTrack = &tracks[0];

    guiPlayPause = gui->addToggle("Play");
    guiPlayPause->onToggleEvent( this, &SequenceMode::onPlayPause);
    guiPlayPause->setChecked(true);

    guiBreak1 = gui->addBreak();
    guiBreak1->setBackgroundColor(ColorPaletteGenerator::stripeMainColor);

    guiOffset = gui->addSlider("Offset",0, QTY_STEPS -1, 0);
    guiOffset->onSliderEvent( this, &SequenceMode::onOffsetChange );
    guiOffset->setPrecision(0);

    guiProbability = gui->addSlider("Probability",0, 1, 1);
    guiProbability->onSliderEvent( this, &SequenceMode::onProbabilityChange );

    guiTempo = gui->addToggle("Tempo");
    guiTempo->setEnabled(false);

    guiUnit = gui->addDropdown( strUnits[0],
        vector<string>( strUnits, strUnits + sizeof(strUnits) / sizeof(string) ) );
    guiUnit->onDropdownEvent(this, &SequenceMode::onChosenUnit);

    guiBreak2 = gui->addBreak();
    guiBreak2->setBackgroundColor(ColorPaletteGenerator::stripeMainColor);

    guiBPM = gui->addSlider("BPM", 60, 180, DEFAULT_TEMPO);
    guiBPM->onSliderEvent(this, &SequenceMode::onBPMChange);

    guiUseMidiClock = gui->addToggle("Use MIDI Clock");
    guiUseMidiClock->onToggleEvent( this, &SequenceMode::onUseMidiClock );


    guiComponents.push_back(guiBPM);
    guiComponents.push_back(guiUseMidiClock);
    guiComponents.push_back(guiTempo);
    guiComponents.push_back(guiTracks);
    guiComponents.push_back(guiUnit);
    guiComponents.push_back(guiPlayPause);
    guiComponents.push_back(guiOffset);
    guiComponents.push_back(guiProbability);
    guiComponents.push_back(guiBreak1);
    guiComponents.push_back(guiBreak2);
}

Json::Value SequenceMode::save() {
    Json::Value root = Json::Value( Json::objectValue );
    Json::Value sequences = Json::Value( Json::arrayValue );

    for ( int i = 0 ; i < QTY_TRACKS ; i++ ) {
        SequenceModeTrack * track = &tracks[i];
        Json::Value sequence = Json::Value( Json::arrayValue );

        for ( int j = 0 ; j < track->secuencia.size() ; j++ ) {
            sequence.append( track->secuencia[j]->getFileName() );
        }

        sequences.append(sequence);
    }

    root["sequences"] = sequences;
    return root;
}
void SequenceMode::load(Json::Value jsonData) {
    Json::Value sequences = jsonData["sequences"];
    if ( sequences != Json::nullValue ) {
        for ( int i = 0 ; i < sequences.size() ; i++ ) {
            Json::Value sequence = sequences[i];
            for ( int j = 0 ; j < sequence.size() ; j++ ) {
                string filename = sequence[j].asString();
                tracks[i].toggleSound( sounds->getSoundByFilename(filename) );
            }
            tracks[i].playing = false;
        }

        sounds->allSoundsSelectedOff();
    }
}

void SequenceMode::onPlayPause(ofxDatGuiToggleEvent e) {
    if ( selectedTrack != NULL ) {
        selectedTrack->playing = !selectedTrack->playing;
    }
}

void SequenceMode::onUseMidiClock(ofxDatGuiToggleEvent e)
{
    if ( e.checked ) {
        tempo.stop();
    } else {
        tempo.start(true);
    }

}

void SequenceMode::onChosenUnit(ofxDatGuiDropdownEvent e) {
    if ( selectedTrack != NULL ) {
        selectedTrack->setCantSteps( intUnits[ e.child ] );
    }
}

void SequenceMode::onMatrixEvent(ofxDatGuiMatrixEvent e) {
    for ( unsigned int i = 0 ; i < QTY_TRACKS ; i++ ) {
        tracks[i].unselectAllSounds();
    }
    selectedTrack = &tracks[ e.child ];
    selectedTrack->selectAllSounds();

    guiPlayPause->setChecked( selectedTrack->playing );
    guiOffset->setValue( selectedTrack->offset );
    guiProbability->setValue( selectedTrack->probability );

    for ( unsigned int i = 0 ; i < QTY_UNITS ; i++ ) {
        if ( selectedTrack->cantSteps == intUnits[i] ) {
            guiUnit->setLabel( strUnits[i] );
            break;
        }
    }
}

void SequenceMode::onBPMChange(ofxDatGuiSliderEvent e)
{
    int milis = (( 60.0 / e.value ) * 1000.0);
    tempo.setup( milis / 4.0);
}

void SequenceMode::onOffsetChange(ofxDatGuiSliderEvent e)
{
    if ( selectedTrack != NULL ) {
        selectedTrack->setOffset( e.value );
    }
}

void SequenceMode::onProbabilityChange(ofxDatGuiSliderEvent e)
{
    if ( selectedTrack != NULL ) {
        selectedTrack->setProbability( e.value );
    }
}

void SequenceMode::onSelectedMode() {
    SequenceMode::isModeActive = true;

    if ( selectedTrack != NULL ) {
        selectedTrack->selectAllSounds();
    }
}
void SequenceMode::onUnselectedMode() {
    SequenceMode::isModeActive = false;

    sounds->allSoundsSelectedOff();
}

void SequenceMode::update() {
    tempo.update();
}
void SequenceMode::draw() {
    // for ( unsigned int i = 0 ; i < QTY_TRACKS ; i++ ) {
    //     tracks[i].draw( &tracks[i] != selectedTrack );
    // }
}
void SequenceMode::beforeDraw() {
    for ( unsigned int i = 0 ; i < QTY_TRACKS ; i++ ) {
        tracks[i].draw( &tracks[i] != selectedTrack );
    }
}

void SequenceMode::onTempo(int &args) {
    currentBeat++;

    //el tempo marca semicorcheas
    if ( currentBeat > 15 ) {
        currentBeat = 0;
    }

    if ( selectedTrack != NULL ) {
        if ( SequenceMode::currentBeat % 4 == 0 ) {
            int negra = SequenceMode::currentBeat / 4 + 1;
            guiTempo->setLabel( "Tempo: " + ofToString(negra) );
            guiTempo->setChecked(true);
        } else {
            guiTempo->setChecked(false);
        }
    }


    for ( unsigned int i = 0 ; i < QTY_TRACKS ; i++ ) {
        tracks[i].onTempo();
    }


}

void SequenceMode::mousePressed(ofVec2f p, int button) {
    Sound * hoveredSound = sounds->getHoveredSound();
    if ( hoveredSound != NULL ) {
        if ( button == 0 ) {
            selectedTrack->toggleSound( hoveredSound );
        } else if ( button == 2 ) {
            sounds->playSound( hoveredSound );
        }
    }
}

void SequenceMode::mouseDragged(ofVec2f p, int button) {
    if ( button == 2 ) {
        Sound * hoveredSound = sounds->getHoveredSound();

        if ( hoveredSound != NULL ) {
            selectedTrack->processSequence(); //habria que poner un threshold capaz ?
        }
    }
}

void SequenceMode::midiMessage(Utils::midiMsg m)
{
    if ( m.status == "Time Clock" ) {
        if ( guiUseMidiClock->getChecked() ) {
            ofLog() << m.beats;
            int args = 0;
            currentBeat = m.beats - 1;
            onTempo(args);
        }
    }
};

//////////////////////
// SequenceModeTrack
//////////////////////

Sounds * SequenceModeTrack::sounds = NULL;

//Esto se llama una vez por semiCorchea
void SequenceModeTrack::onTempo() {
    if ( currentStep == -1 ) {
        currentStep = SequenceMode::currentBeat;
    } else {
        currentStep++;
        if ( currentStep >= cantSteps ) {
            currentStep = 0;
        }
    }

    //esto es que se resincronizo, ej. alguien le dio play desde otro programa
    //acá sucede que si alguien le da play justo en el último step, la secuencia no se reiniciará (pero estará a tempo)
    if ( SequenceMode::currentBeat == 0 && lastStep != (QTY_STEPS-1) ) {
        currentStep = 0;
    }

    if ( playing && secuenciaEnTiempo[currentStep] != NULL ) {
        if ( ofRandom(0,1) < probability ) {
            sounds->playSound( secuenciaEnTiempo[currentStep] );
        }
    }
    lastStep = SequenceMode::currentBeat;
}

void SequenceModeTrack::toggleSound(Sound * sound) {
    if ( sound != NULL ) {
        if ( !sound->selected ) {
            secuencia.push_back( sound );
            sound->selected = true;
        } else {
            secuencia.erase(
                std::remove(secuencia.begin(),
                secuencia.end(), sound), secuencia.end());

            sound->selected = false;

        }

        // ofLog() << secuencia.size() << endl;
        processSequence();
    }
}

SequenceModeTrack::SequenceModeTrack() {
    secuenciaEnTiempo = new Sound*[cantSteps];

    for ( int i = 0 ; i < cantSteps ; i++ ) {
        secuenciaEnTiempo[i] = NULL;
    }
}

void SequenceModeTrack::selectAllSounds() {
    for ( unsigned int i = 0 ; i < secuencia.size() ; i++ ) {
        secuencia[i]->selected = true;
    }
}
void SequenceModeTrack::unselectAllSounds() {
    for ( unsigned int i = 0 ; i < secuencia.size() ; i++ ) {
        secuencia[i]->selected = false;
    }
}

void SequenceModeTrack::setCantSteps(int value) {
    cantSteps = value;
    processSequence();
}

void SequenceModeTrack::setOffset(int value)
{
    offset = value;
    processSequence();
}

void SequenceModeTrack::setProbability(float value)
{
    probability = value;
}

void SequenceModeTrack::draw( bool notSelected ) {
    if ( isProcessing || currentStep == -1 ) {
        return;
    }

    ofSetLineWidth(1);

    if ( !notSelected && SequenceMode::isModeActive ) {
        ofSetColor(ofColor(255,255,255));
    } else {
        ofSetColor(ofColor(80,80,80));
    }

    polyLine.draw();

    if ( playing ) {
        ofSetColor(ofColor(255,255,255));
        ofFill();

        if ( secuencia.size() > 0 ) {
            ofPoint p;

            if ( secuenciaEnTiempo[currentStep] != NULL ) {
//                p = secuenciaEnTiempo[currentStep]->getPosition();
            } else {
                float offsetedStep = ( currentStep + ( cantSteps - offset ) ) % cantSteps;
                p = polyLine.getPointAtPercent( offsetedStep / cantSteps );
            }
            ofDrawEllipse( p.x , p.y, 5, 5 );
        }

        // ofSetColor(ofColor(255,0,0));
        // ofDrawBitmapString( ofToString(tempo-1) , p.x, p.y);
    }
}

void SequenceModeTrack::processSequence() {
    isProcessing = true;
    polyLine.clear();

    secuenciaEnTiempo = new Sound*[cantSteps];

    for ( int i = 0 ; i < cantSteps ; i++ ) {
        secuenciaEnTiempo[i] = NULL;
    }

    if ( secuencia.size() == 0 ) {
        isProcessing = false;
        return;
    }

    // saco distancia total
    vector<float> distancias;
    float distanciaAcumulada = 0;
    for ( unsigned int i = 1 ; i < secuencia.size() ; i++ ) {
        float distancia = Utils::distance( secuencia[i-1]->getPosition(), secuencia[i]->getPosition() );
        distancias.push_back(distancia);
        distanciaAcumulada += distancia;
    }

    // if ( secuencia.size() == 2 ) {
        float distanciaUltimoConPrimero = Utils::distance( secuencia[secuencia.size()-1]->getPosition(), secuencia[0]->getPosition() );
        distanciaAcumulada += distanciaUltimoConPrimero;
    // }

//     cout << "Distancias iniciales: ";
//     for ( unsigned int i = 0 ; i < distancias.size() ; i++ ) {
//         cout << distancias[i] << ",";
//     }
//     cout << endl;

    secuenciaEnTiempo[0] = secuencia[0];

    int stepsAcumulados = 0;

    for ( unsigned int i = 0 ; i < distancias.size() ; i++ ) {
        distancias[i] = distancias[i] / distanciaAcumulada;
        int relacionDistanciaSteps = floor(distancias[i] * cantSteps); //aca es en relacion con cantSteps no QTY_STEPS

        if (relacionDistanciaSteps == 0) {
            relacionDistanciaSteps = 1;
        }

        distancias[i] = relacionDistanciaSteps + stepsAcumulados;

        stepsAcumulados += relacionDistanciaSteps;
    }

//     cout << "Distancias finales: ";
    for ( unsigned int i = 0 ; i < distancias.size() ; i++ ) {
//         cout << distancias[i] << ",";
        int step = distancias[i];
        secuenciaEnTiempo[ step ] = secuencia[i+1];
    }
//     cout << endl;

//    cout << "secuenciaEnTiempo: [";
//    for ( unsigned int i = 0 ; i < cantSteps ; i++ ) {
//        if ( secuenciaEnTiempo[i] == NULL ) {
//            cout << "0 ";
//        } else {
//            cout << "1 ";
//        }

//    }
//    cout << "]" << endl;

    for ( unsigned int i = 0 ; i < secuencia.size() ; i++ ) {
        polyLine.addVertex( secuencia[i]->getPosition().x,
                            secuencia[i]->getPosition().y, 0 );
    }

    polyLine.close();

    processSequenceOffset();

    isProcessing = false;
}

void SequenceModeTrack::processSequenceOffset()
{
    Sound ** secuenciaEnTiempoOffset = new Sound*[cantSteps];

    for ( int i = 0 ; i < cantSteps ; i++ ) {
        secuenciaEnTiempoOffset[ (i + offset) % cantSteps ] = secuenciaEnTiempo[i];
    }

    secuenciaEnTiempo = secuenciaEnTiempoOffset;

//    cout << "secuenciaEnTiempoOffset: [";
//    for ( unsigned int i = 0 ; i < cantSteps ; i++ ) {
//        if ( secuenciaEnTiempoOffset[i] == NULL ) {
//            cout << "0 ";
//        } else {
//            cout << "1 ";
//        }

//    }
//    cout << "]" << endl;
}
