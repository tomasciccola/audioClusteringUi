#pragma once

#include "ofMain.h"
#include "SessionManager.h"
#include "Utils.h"
#include "Sound.h"
#include "Gui.h"
#include "Modes.h"
#include "CamZoomAndPan.h"
#include "MidiServer.h"
#include "DBScan.h"
#include "ColorPaletteGenerator.h"

class ofApp : public ofBaseApp {
public:
    void setup();
    void update();
    void draw();

    void drawFps();

    void keyPressed(int key);
    void keyReleased(int key);
    void mousePressed(int x, int y, int button);
    void mouseScrolled(int x, int y, float scrollX, float scrollY);
    void mouseDragged(int x, int y, int button);
    void mouseReleased(int x, int y, int button);
    void mouseMoved(int x, int y);

    void makeSessionGui();
    void savePressed(ofxDatGuiButtonEvent e);

    Sounds* sounds = NULL;
    Modes *modes = NULL;
    MidiServer *midiServer = NULL;
    Gui* gui = NULL;

    char cwd[1024];
    vector<string> arguments;

    ofxJSONElement jsonFile;

    CamZoomAndPan cam;
    // ofxInfiniteCanvas cam;
    bool drawGui = true;
    bool useCam = true;

    ofTrueTypeFont font;

};
