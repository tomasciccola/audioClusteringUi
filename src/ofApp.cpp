#include "ofApp.h"
//--------------------------------------------------------------
void ofApp::setup() {

    ofSetWindowTitle("AudioStellar");
    ofEnableAlphaBlending();
    ofSetVerticalSync(true);

    gui = new Gui();

    //ofAddListener(gui->onLoadJsonButtonPressed, this, &ofApp::onLoadJsonButtonPressed);
    
    jsonFile = SessionManager::loadJson(arguments);
    sounds = new Sounds(jsonFile,gui->getLeftPanel());

    midiServer = new MidiServer(gui->getLeftPanel());

    modes = new Modes(sounds,gui->getRightPanel());
    gui->setModes(modes);

    SessionManager::loadSession(modes, sounds);
    makeSessionGui();

    midiServer->init(modes);
    cam.init();

    font.load( OF_TTF_MONO, 10 );

//    ofSoundStream soundStream;
//    ofSoundStreamSettings sSettings;
//    auto devices = soundStream.getDeviceList(ofSoundDevice::Api::JACK);

//    sSettings.setOutDevice(devices[0]);

//    ofSoundStreamSetup(sSettings);
//    ofSoundStreamListDevices();
}

//--------------------------------------------------------------
void ofApp::update() {

    sounds->update();
    modes->update();
    gui->update();
    midiServer->update();
}

//--------------------------------------------------------------
void ofApp::draw() {
    ofBackground(25);
//    ofBackground(0);

    if ( useCam ) {
        cam.begin();
    }

    modes->beforeDraw();
    sounds->draw();
    modes->draw();

    if ( useCam ) {
        cam.end();
    }

    gui->draw();

    drawFps();
}

void ofApp::drawFps()
{
    ofSetColor(255);
    font.drawString(
                "FPS: " + ofToString(ofGetFrameRate(), 0),
                ofGetWidth() - 70,
                ofGetHeight() - 10);
}

void ofApp::makeSessionGui() {
    ofxDatGuiButton* btnSave = gui->getLeftPanel()->addButton("Save session");
    btnSave->onButtonEvent(this, &ofApp::savePressed);
    btnSave->setStripeColor(ColorPaletteGenerator::stripeMainColor);
}
void ofApp::savePressed(ofxDatGuiButtonEvent e) {
    SessionManager::saveSession(modes, sounds);
}


void ofApp::keyPressed(int key) {
    gui->keyPressed(key);
    modes->keyPressed(key);

}

void ofApp::mousePressed(int x, int y, int button) {
    ofVec3f realCoordinates = cam.screenToWorld(ofVec3f(x,y,0));
    ofVec3f screenCoords(x,y,0);

    if(!gui->onGui(screenCoords)) {
        modes->mousePressed( realCoordinates.x, realCoordinates.y, button );
    }

    gui->mousePressed(x,y,button);
}

void ofApp::mouseMoved(int x, int y) {
    ofVec3f realCoordinates = cam.screenToWorld(ofVec3f(x,y,0));
    ofVec3f screenCoords(x,y,0);

    sounds->mouseMoved(realCoordinates.x,realCoordinates.y, gui->onGui(screenCoords) );

    if(!gui->onGui(screenCoords)) {
        modes->mouseMoved(realCoordinates.x,realCoordinates.y);
    }

}

void ofApp::mouseDragged(int x, int y, int button) {
    ofVec3f realCoordinates = cam.screenToWorld(ofVec3f(x,y,0));
    ofVec3f screenCoords(x,y,0);


    if(!gui->onGui(screenCoords)) {
        modes->mouseDragged(realCoordinates.x,realCoordinates.y,button);
        sounds->mouseDragged(realCoordinates, button);
    }
}

void ofApp::mouseReleased(int x, int y, int button) {
    ofVec3f realCoordinates = cam.screenToWorld(ofVec3f(x,y,0));
    if(!gui->onGui(realCoordinates)){
        modes->mouseReleased(realCoordinates.x,realCoordinates.y,button);
    }
}

void ofApp::keyReleased(int key) {
    if ( key == 'c' ) {
        useCam = !useCam;
    }

}
void ofApp::mouseScrolled(int x, int y, float scrollX, float scrollY) {
    // cam.mouseScrolled(x,y,scrollX,scrollY);
}
