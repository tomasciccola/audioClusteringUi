#pragma once

#include "ofMain.h"
#include "ofxDatGui.h"
#include "Modes.h"

class Gui {

private:

    ofxDatGui *generalPanel;
    ofxDatGui *modesPanel;
    vector<ofxDatGui*> panels;

    bool drawGui = true;

    bool isGuiVisible();
    bool insideGuiBounds(ofVec3f p);
    void welcomeScreen();

    Modes * modes;
public:

    Gui();
    void draw();
    void update();
    ofxDatGui * getLeftPanel();
    ofxDatGui * getRightPanel();
    bool onGui(ofVec3f p);
    void keyPressed(int key);

    void mousePressed(int x, int y, int button);

    //void makeSessionGui();

    void setModes(Modes * modes);
};
