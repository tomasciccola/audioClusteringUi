#pragma once
#include "ofMain.h"

class dbscanPoint {
public:
    double x, y;
    int ptsCnt, cluster;
    double getDis(const dbscanPoint & ot) {
        return sqrt((x-ot.x)*(x-ot.x)+(y-ot.y)*(y-ot.y));
    }
};

class DBScan
{
private:
    void dfs (int now, int c);
    void checkNearPoints();
    bool isCoreObject(int idx);
public:
    const int NOISE = -2;
    const int NOT_CLASSIFIED = -1;

    int minPts;
    double eps;
    vector<dbscanPoint> points;
    int size;
    vector<vector<int> > adjPoints;
    vector<bool> visited;
    vector<vector<int> > cluster;
    int clusterIdx;

    DBScan(double eps, int minPts, vector<ofPoint> points);
    void run();
    vector<vector<int>> getCluster();
};
