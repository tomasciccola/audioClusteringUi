#include "Gui.h"

Gui::Gui() {
    generalPanel = new ofxDatGui(ofxDatGuiAnchor::TOP_LEFT);
    generalPanel->setAutoDraw(false);
    panels.push_back(generalPanel);

    modesPanel = new ofxDatGui(ofxDatGuiAnchor::TOP_RIGHT);
    modesPanel->setAutoDraw(false);
    panels.push_back(modesPanel);
}

void Gui::update() {
    if(drawGui) {
        for(int i = 0; i<panels.size(); i++) {
            panels[i]->update();
        }
    }
}

void Gui::draw() {
    if(drawGui) {
        for(int i = 0; i<panels.size(); i++) {
            panels[i]->draw();
        }

        float currentX = ofGetWidth()/2 - modes->modes.size()*ImageButton::buttonSize/2 ;
        float stepX = ImageButton::buttonSize;
        for(auto &imgButton: modes->modesButtons) {
            imgButton.draw(currentX, 0);
            currentX += stepX;
        }
    }
}
ofxDatGui * Gui::getLeftPanel() {
    return generalPanel;
}
ofxDatGui * Gui::getRightPanel() {
    return modesPanel;
}

bool Gui::insideGuiBounds(ofVec3f p) {
    for(int i = 0; i<panels.size();i++){
        ofxDatGui *gui= panels[i];

        ofRectangle guiRect(gui->getPosition().x, gui->getPosition().y,
                            gui->getWidth(), gui->getHeight());
        if ( guiRect.inside( p.x, p.y ) ) {
            return true;
        }
    }

    for ( int i = 0 ; i < modes->modesButtons.size() ; i++ ) {
        if ( modes->modesButtons[i].click(p.x,p.y) ) {
            return true;
        }
    }

    return false;

}
bool Gui::isGuiVisible() {
    return drawGui;
}
bool Gui::onGui(ofVec3f p) {
    return isGuiVisible() && (isGuiVisible() && insideGuiBounds(p));
}
void Gui::keyPressed(int key) {
    if(key == 'g') {
        drawGui = !drawGui;
    }
}

void Gui::mousePressed(int x, int y, int button)
{
    for ( int i = 0 ; i < modes->modesButtons.size() ; i++ ) {
        if ( modes->modesButtons[i].click(x,y) ) {
            modes->modeSelected(i);
        }
    }
}

void Gui::setModes(Modes *modes)
{
    this->modes = modes;
}
