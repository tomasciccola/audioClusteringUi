#ifndef _AREADRAWER
#define _AREADRAWER

#include "ofMain.h"
#include "Utils.h"

class AreaDrawer:public ofPolyline{
private:
    ofVec2f initialPoint;

public:
    AreaDrawer(ofVec2f _initP);
    ofVec2f getInitialPoint();
    bool shouldKeepArea;
};

class AreaDrawerManager{
private:
    vector<AreaDrawer*> areas;
    AreaDrawer* area = NULL;
    AreaDrawer* prevArea = NULL;

    string status;
public:
    AreaDrawerManager();
    void draw();
    void update(ofVec2f p);
    void mouseReleased(ofVec2f p);
    float getArea();
    ofPoint getCentroid();
    string getStatus();
    void noteSetted(bool s);
    bool assignMode = false;

};

#endif
