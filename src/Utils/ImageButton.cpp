#include "ImageButton.h"

int ImageButton::buttonSize = 40;

bool ImageButton::getActive() const
{
    return active;
}

void ImageButton::setActive(bool value)
{
    active = value;
}

ImageButton::ImageButton(float x, float y, string path, string pathActive)
{
    this->x = x;
    this->y = y;

    imgButton = new ofImage( path );
    imgButtonActive = new ofImage( pathActive );
    setActive(false);
}

void ImageButton::draw()
{
    ofFill();
    ofSetColor(backgroundColor);

    ofDrawRectangle(x,y, buttonSize, buttonSize);

    ofSetColor(255);
    if ( !getActive() ) {
        imgButton->draw(x,y);
    } else {
        imgButtonActive->draw(x,y);
    }
}

void ImageButton::draw(float x, float y) {
    this->x = x;
    this->y = y;
    draw();
}

bool ImageButton::click(float mouseX, float mouseY)
{
    if ( mouseX >= x && mouseX <= x + imgButton->getWidth() ) {
        if ( mouseY >= y && mouseY <= y + imgButton->getHeight() ) {
            return true;
        }
    }

    return false;
}


