#ifndef _SESSIONMANAGER
#define _SESSIONMANAGER

#include "ofMain.h"
#include "Utils.h"
#include "ofxJSON.h"
#include "ofxDatGui.h"
#include "Modes/Modes.h"

namespace SessionManager {

    static string jsonFilename;
    static ofxJSONElement jsonFile;

    static ofxJSONElement loadJson(vector<string> args) {
        bool osx = false;
        #ifdef TARGET_OSX
        osx = true;
        #endif
        if(args.size() > 1 && !osx) {
            jsonFilename = args.at(1);
        } else {

            ofFileDialogResult selection =
                ofSystemLoadDialog("Seleccione archivo json");

            if(selection.bSuccess) {
                jsonFilename = selection.getPath();
            } else {
                ofLogVerbose("User canceled selection");
            }
        }

        bool loadedJson = jsonFile.open(jsonFilename);

        if(!loadedJson) {
            ofLogNotice("ERROR", "problemas al cargar el json");
            ofExit();
        }

        jsonFile["audioFilesPath"] = Utils::resolvePath
                                     (jsonFilename, jsonFile["audioFilesPath"].asString());

        return jsonFile;

    }

    static void loadSession(Modes * modes, Sounds * sounds) {
        Json::Value jsonModes = jsonFile["modes"];
        if ( jsonModes != Json::nullValue ) {
            for ( int i = 0 ; i < modes->modes.size() ; i++ ) {
                modes->modes[i]->load( jsonModes[ modes->modeNames[i] ] );
            }
        }

        sounds->load(jsonFile["sounds"]);
    }

    static void saveSession(Modes * modes, Sounds * sounds) {
        ofLog() << "Saving...";

        /****
            Sounds
        ****/

        Json::Value jsonSounds = jsonFile["sounds"];

        if ( jsonSounds == Json::nullValue ) {
            jsonFile["sounds"] = Json::Value( Json::objectValue );
        }

        jsonFile["sounds"] = sounds->save();


        /****
            MODES
        ****/
        Json::Value jsonModes = jsonFile["modes"];

        if ( jsonModes == Json::nullValue ) {
            jsonFile["modes"] = Json::Value( Json::objectValue );
        }

        for ( int i = 0 ; i < modes->modeNames.size() ; i++ ) {
            jsonFile["modes"][ modes->modeNames[i] ] = modes->modes[i]->save();
        }

        // jsonFile.save("test.json", true);
        jsonFile.save(jsonFilename, true);

        ofLog() << "Done saving";
    }
};

#endif
