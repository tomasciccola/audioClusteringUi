#include "MidiServer.h"

bool MidiServer::midiLearn = false;

MidiServer::MidiServer(ofxDatGui *_gui) {
    gui = _gui;

    //si hay un error aca, dale git pull a addons/ofxMidi
    midiDevices = midiIn.getInPortList();

    //check for midi devices
    deviceChecker.setup(1000);
    deviceChecker.start(true);

    ofAddListener( deviceChecker.TIMER_COMPLETE, this, &MidiServer::checkForNewDevices ) ;

    midiIn.setVerbose(true);
    midiIn.addListener(this);
    setupGui();

}

void MidiServer::init(Modes *_modes){
  modes = _modes;
}

void MidiServer::update(){
    deviceChecker.update();
    //ya que dentro de cada mode sólo seteo el valor de la variable y no el estado del toggle
    btnMidiLearn->setChecked(midiLearn);
}

void MidiServer::setupGui() {
    //Selector de dispositivo
    midiDeviceSelector = gui->addDropdown("Midi Devices", midiDevices);
    midiDeviceSelector->onDropdownEvent(this, &MidiServer::onMidiDeviceSelected);
    midiDeviceSelector->setStripeColor(ColorPaletteGenerator::stripeMainColor);

    //midiLearn
    btnMidiLearn = gui->addToggle("Midi Learn");
    btnMidiLearn->onToggleEvent(this, &MidiServer::onMidiLearn);
    btnMidiLearn->setStripeColor(ColorPaletteGenerator::stripeMainColor);
    btnMidiLearn->setChecked(MidiServer::midiLearn);

    gui->addBreak()->setBackgroundColor(ColorPaletteGenerator::stripeMainColor);

}
void MidiServer::onMidiDeviceSelected(ofxDatGuiDropdownEvent e) {
    //primero cerrá el puerto del device anterior
    midiIn.closePort();
    currentMidiDevice = midiDevices[e.child];
    //seteá el label actual
    midiDeviceSelector->setLabel("DEVICE: " + currentMidiDevice);
    //abrí el nuevo puerto
    midiIn.openPort(currentMidiDevice);
    midiIn.ignoreTypes(true,false,true); //no ignores los de clock
}

void MidiServer::onMidiLearn(ofxDatGuiToggleEvent e){
   MidiServer::midiLearn = !MidiServer::midiLearn;
}

void MidiServer::checkForNewDevices(int &args){
    midiDevices = midiIn.getInPortList();
    //ofLogNotice("Check", ofToString(midiDevices));
}

void MidiServer::newMidiMessage(ofxMidiMessage& msg) {
    Utils::midiMsg m;

//    ofLog() << "Message" << ofxMidiMessage::getStatusString(msg.status);

    // Los status puede ser:
    // Time Clock / Time Code / Note On / Note Off / Control Change
    //TODO: Guarda que hay Constantes !! No usar String

    m.status = ofxMidiMessage::getStatusString(msg.status);
    m.pitch =  msg.pitch;
    m.cc = msg.control;
    m.ccVal = msg.value;
    m.beats = -1;
    m.bpm = -1;

    if(clock.update(msg.bytes)) {

        // a MIDI beat is a 16th note, so do a little math to convert to a time signature:
        // 4/4 -> 4 notes per bar & quarter note = 1 beat, add 1 to count from 1 instead of 0
        unsigned int beats = clock.getBeats();
        int normalizedBeat = ( beats % 16 ) + 1;
        if ( normalizedBeat != currentBeat ) {
            currentBeat = normalizedBeat;
            currentSyncBeat++;

            m.beats = currentSyncBeat;

            if ( currentSyncBeat >= 15 ) {
                currentSyncBeat = -1;
            }



        } else {
            return; //no envies el mensaje si es repetido
        }

//        int quarters = beats / 4; // convert total # beats to # quarters
//        int bars = (quarters / 4) + 1; // compute # of bars
//        int beatsInBar = (quarters % 4) + 1; // compute remainder as # notes within the current bar
//        m.seconds = clock.getSeconds();
    }

    /*if ( MIDI_TIME_CLOCK ) {
        bpm += (clock.getBpm() - bpm) / 5
    }*/

    if ( msg.status == MIDI_START ) {
        currentSyncBeat = -1;
        ofLog() << "clock started";
    }

    modes->midiMessage(m);
}
