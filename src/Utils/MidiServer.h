#ifndef _MIDISERVER
#define _MIDISERVER

#include "ofMain.h"
#include "ofxDatGui.h"
#include "ofxMidi.h"
#include "ofxMidiClock.h"
#include "Modes.h"
#include "Utils.h"
#include "ColorPaletteGenerator.h"
#include "ofxSimpleTimer.h"

//Esto es feo, pero soluciona un temita del orden de includes
class Modes;

class MidiServer:public ofxMidiListener {
private:
    ofxDatGui *gui = NULL;
    ofxDatGuiDropdown *midiDeviceSelector = NULL;
    ofxDatGuiToggle * btnMidiLearn = NULL;


    Modes *modes = NULL;

    ofxMidiIn midiIn;
    ofxMidiClock clock; //< clock message parser

    vector<string> midiDevices;
    string currentMidiDevice = "Not setted";

    int currentBeat = -1;
    int currentSyncBeat = -1;

    //Checkear si se conectaron nuevos dispositivos midi
    ofxSimpleTimer deviceChecker;
    void checkForNewDevices(int &args);

    void onMidiDeviceSelected(ofxDatGuiDropdownEvent e);
    void onMidiLearn(ofxDatGuiToggleEvent e);
    void newMidiMessage(ofxMidiMessage& eventArgs);
    void setupGui();

public:
    MidiServer(ofxDatGui *_gui);
    void init(Modes *_modes);
    void update();
    ~MidiServer() {}

    static bool midiLearn;

    //    bool clockRunning = false; //< is the clock sync running?
    //    unsigned int beats = 0; //< song pos in beats
    //    double seconds = 0; //< song pos in seconds, computed from beats
    //    double bpm = 120; //< song tempo in bpm, computed from clock length
};
#endif
