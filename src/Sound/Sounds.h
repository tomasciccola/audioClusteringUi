#pragma once

#include "ofMain.h"
#include "Utils.h"
#include "ColorPaletteGenerator.h"
#include "ofxDatGui.h"
#include "Sound.h"
#include "ofxJSON.h"
#include "ofxConvexHull.h"
#include "DBScan.h"

struct soundPosition {
  Sound * sound;
  ofVec2f position;
} ;

class Sounds {
private:
    vector<Sound*> sounds;

    ofBuffer tsvFile;
    ofxDatGui *gui;
    ofxDatGuiToggle *originalPositionsToggle;
    ofxDatGuiLabel *currentSoundLabel = nullptr;
    ofxDatGuiMatrix *matrixClusters = nullptr;
    ofxDatGuiSlider *epsDbScan;
    ofxDatGuiButton *btnDbScan;
    ofxDatGuiFolder * folderClusters;

    void getSpaceLimits(vector<string> soundsCoords);
    ofVec3f spaceLimits[2];
    const int spacePadding = 30;

    set<int> clusterIds;

    bool skipFileHeader = true;//salteate la primera línea al leer el .tsv

    float volumen = 0.5;

    bool replaySound = false;
    int idCount = 0;

#define BG_TRANSLATION 0.3
#define BG_ALPHA_MIN 30
#define BG_ALPHA_MAX 50
#define BG_ALPHA_STEP 0.3
#define BG_GLOW_SIZE 50
    int windowOriginalWidth;
    int windowOriginalHeight;
    ofFbo fboBackground;
    float backgroundAlpha = BG_ALPHA_MIN;
    int backgroundDirection = 1;

    //overrided Positions
    vector<soundPosition> soundPositions;
    void updateSoundPosition(Sound * sound);

    void findConvexHulls();
    vector<ofPolyline> hulls;
public:
    Sounds(ofxJSONElement jsonFile, ofxDatGui *_gui);//, ofCamera * cam);

    // Cada modo debería setear si necesita o no que se checkee hover
    bool hoveredActivated = false;

    Sound * lastPlayedSound = nullptr;

    void update();
    void draw();
    void drawBackground();
    void generateBackground();

    void setupGui();
    void setupGuiClusters();

    void onToggleEvent(ofxDatGuiToggleEvent e);
    void onSliderEvent(ofxDatGuiSliderEvent e);
    void onMatrixEvent(ofxDatGuiMatrixEvent e);
    void onDoClustering(ofxDatGuiButtonEvent e);

    void mouseMoved(int x, int y, bool onGui);
    void mouseDragged(ofVec2f p, int button);

    void playSoundAt(ofVec2f position);
    void playSound(Sound * sound);
    void playSound(int id);
    Sound * getNearestSound(ofVec2f position);

    float getVolume();
    string getCurrentSound();
    void setFilenameLabel(string fileName);

    void allSoundsSelectedOff();
    void allSoundsHoveredOff(Sound * except = NULL);

    void setUseOriginalPositions(bool yes);

    Sound * getHoveredSound();
    Sound * getSoundByFilename(string filename);
    Sound * getSoundById(int id);
    vector<Sound*> getSounds();
    vector<ofPoint> getSoundsAsPoints();

    void setDBScanClusters( vector<dbscanPoint> points );

    string currentSound = "";
    // bool enableHovering = false; Esto debería ser asi

    const int MAX_LOADED_SOUNDS = 100;
    int nLoadedSounds = 0;
    Sound *loadedSounds[100] = {NULL};//llevar la cuenta de los sonidos cargados

    ofxDatGuiToggle * playOnHover = NULL;

    Json::Value save();
    void load( Json::Value jsonData );
};
