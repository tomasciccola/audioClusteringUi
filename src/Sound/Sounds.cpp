#include "Sounds.h"

//vector<int> ColorPaletteGenerator::colors = {0xa6cee3,0x1f78b4,0xb2df8a,0x33a02c,0xfb9a99,0xe31a1c,0xfdbf6f,0xff7f00,0xcab2d6,0x6a3d9a,0xffff99,0xb15928};


Sounds::Sounds(ofxJSONElement jsonFile, ofxDatGui *_gui) {

    gui = _gui;

    string soundsDir = jsonFile["audioFilesPath"].asString();

    vector<string> soundFiles = ofSplitString(jsonFile["tsv"].asString(), "|");

    getSpaceLimits(soundFiles);

    for(int i = 0; i<soundFiles.size() - 1; i++) {
        if(soundFiles[i] == "")continue;
        vector<string>lineElements = ofSplitString(soundFiles[i],",");

        int cluster = ofToInt(lineElements[TSV_FIELD_CLUSTER]);
        string name = lineElements[TSV_FIELD_NAME];

        ofVec3f position;
        position.set(
            ofToFloat(lineElements[TSV_FIELD_X]),
            ofToFloat(lineElements[TSV_FIELD_Y]),
            ofToFloat(lineElements[TSV_FIELD_Z]));

        position.x = ofMap(position.x, spaceLimits[1].x, spaceLimits[0].x, 0 + spacePadding,
                           ofGetWidth() - spacePadding);
        position.y = ofMap(position.y, spaceLimits[1].y, spaceLimits[0].y, 0 + spacePadding,
                           ofGetHeight() - spacePadding);
        position.z = ofMap(position.z, spaceLimits[1].z, spaceLimits[0].z, 0.5,1);

        string file = soundsDir + name;

        Sound *sound = new Sound(file, position, cluster);
        sound->id = idCount;
        idCount++;

        // find N_CLUSTERS
//        for (int i = 0; i < clusterIds.size(); i++) {
//            if (cluster == clusterIds[i]) {
//                uniqueClusterId = false;
//                break;
//            }
//            uniqueClusterId = true;
//        }
//        if (uniqueClusterId) {
//            clusterIds.push_back(cluster);
//        }
        sound->setMultiPlay(false);
        sounds.push_back(sound);
    }

    windowOriginalWidth = ofGetWidth();
    windowOriginalHeight = ofGetHeight();

    setupGui();
}

void Sounds::update() {
    for (int i = 0; i < sounds.size(); i++) {
        sounds[i]->update(volumen);
    }
}

void Sounds::draw() {
    ofFill();

    drawBackground();

    ofSetColor(255,255,255,255);

//    for ( int i = 0 ; i < hulls.size() ; i++ ) {
//        hulls[i].draw();
//    }

    //ofSetCircleResolution(100);
    for (int i = 0; i < sounds.size(); i++) {
       if(!sounds[i]->getHide()) {
            sounds[i]->draw();
       }
    }
}

void Sounds::drawBackground()
{
    ofPushMatrix();
    ofTranslate( -windowOriginalWidth*BG_TRANSLATION, -windowOriginalHeight*BG_TRANSLATION );
    ofSetColor(255,255,255,round(backgroundAlpha));
    fboBackground.draw(0,0);

    backgroundAlpha += BG_ALPHA_STEP * backgroundDirection * ofGetLastFrameTime() * 40;

    if ( backgroundAlpha >= BG_ALPHA_MAX ) {
        backgroundDirection = -1;
    } else if ( backgroundAlpha <= BG_ALPHA_MIN ) {
        backgroundDirection = 1;
    }
    ofPopMatrix();
}

void Sounds::generateBackground()
{
    if ( !fboBackground.isAllocated() ) {
        fboBackground.allocate(windowOriginalWidth * 1.5, windowOriginalHeight * 1.5, GL_RGBA);
    }

    fboBackground.begin();
        ofClear(25,0);
        ofPushMatrix();
        ofTranslate( windowOriginalWidth * BG_TRANSLATION, windowOriginalHeight * BG_TRANSLATION );

        float glowSize = SIZE_ORIGINAL * BG_GLOW_SIZE;

        for ( auto * sound : sounds ) {
            if ( sound->getCluster() < 0 ) {
                continue;
            }
            ofSetColor( ColorPaletteGenerator::getColor(sound->getCluster()) );
//            ofDrawCircle(position.x, position.y, glowBigSize);
            Sound::imgGlow.draw(sound->position.x - glowSize / 2,
                                sound->position.y - glowSize / 2,
                                glowSize,
                                glowSize);
        }

        ofNoFill();
        ofSetColor(255,0,0);
        ofPopMatrix();
//        ofDrawRectangle(1,1,fboBackground.getWidth()-2, fboBackground.getHeight()-1);
    fboBackground.end();
}

void Sounds::findConvexHulls() {
    ofxConvexHull convexHull;
    vector<ofVec3f> cluster;
    string strDebug = "clusterIDS: ";

    hulls.clear();

    for ( auto clusterId : clusterIds ) {
        strDebug += ofToString(clusterId) + ",";
        cluster.clear();
        for ( auto * sound : sounds ) {
            if ( sound->getCluster() == clusterId ) {
                cluster.push_back( sound->getPosition() );
            }
        }

        ofPolyline polyHull;
        if ( cluster.size() >= 3 ) {
            vector<ofPoint> hull = convexHull.getConvexHull( cluster );

            for ( int i = 0 ; i < hull.size() ; i++ ) {
                polyHull.addVertex( hull[i] );
            }
            polyHull.close();

    //        agrando desde el centro
            for( int i = 0 ; i < polyHull.size() ; i++ ) {
                auto &p = polyHull.getVertices()[i];

                p.x += polyHull.getNormalAtIndex(i).x * 15;
                p.y += polyHull.getNormalAtIndex(i).y * 15;
            }
        }

        hulls.push_back( polyHull );
    }
//    ofLog() << strDebug;

//    int currentCluster = 0;
//    ofxConvexHull convexHull;
//    vector<ofVec3f> cluster;

//    hulls.clear();

//    do {
//        cluster.clear();
//        for (int i = 0; i < sounds.size(); i++) {
//            if ( sounds[i]->getCluster() == currentCluster ) {
//                cluster.push_back( sounds[i]->getPosition() );
//            }
//        }

//        if ( cluster.size() > 0 ) {
//            vector<ofPoint> hull = convexHull.getConvexHull( cluster );
//            ofPolyline polyHull;
//            for ( int i = 0 ; i < hull.size() ; i++ ) {
//                polyHull.addVertex( hull[i] );
//            }

//            polyHull.close();

//            //agrando desde el centro
////            for( int i = 0 ; i < polyHull.size() ; i++ ) {
////                auto &p = polyHull.getVertices()[i];

////                p.x += polyHull.getNormalAtIndex(i).x * 15;
////                p.y += polyHull.getNormalAtIndex(i).y * 15;
////            }

//            // polyHull = polyHull.getSmoothed(2, 0.5);
//            hulls.push_back( polyHull );
//        }

//        currentCluster++;
//    } while( cluster.size() > 0 );
}

void Sounds::updateSoundPosition(Sound * sound) {
    soundPosition * found = NULL;
    for ( int i = 0 ; i < soundPositions.size() ; i++ ) {
        if ( soundPositions[i].sound == sound ) {
            found = &soundPositions[i];
            break;
        }
    }

    if ( found == NULL ) {
        found = new soundPosition;
        found->sound = sound;

        soundPositions.push_back(*found);
    }

    found->position.x = sound->position.x;
    found->position.y = sound->position.y;
}

void Sounds::mouseMoved(int x, int y, bool onGui) {
    if ( onGui ) {
        for ( auto * sound : sounds ) {
            sound->clusterIsHovered = true;
        }
        return;
    }

    if ( hoveredActivated ) {
        Sound * nearestSound = getNearestSound( ofVec2f(x,y) );
        allSoundsHoveredOff(nearestSound);
        if (nearestSound != NULL) {
            if ( !nearestSound->getHovered() ) {
                nearestSound->setHovered(true);

                if ( playOnHover->getChecked() ) {
                    playSound( nearestSound );
                }
            }
        }
    }

    set<int> selectedClusters;
    int selectedCluster = -1;
    for ( int i = 0 ; i < hulls.size() ; i++ ) {
        if ( hulls[i].size() >= 3 && hulls[i].inside( x, y) ) {
            selectedClusters.insert(i);
        }
    }

    for ( auto cluster : selectedClusters ) {
        if ( selectedCluster == -1 ) {
            selectedCluster = cluster;
        } else {
            if ( hulls[cluster].getArea() < hulls[selectedCluster].getArea() ) {
                selectedCluster = cluster;
            }
        }
    }

    for ( auto * sound : sounds ) {
        if ( selectedCluster == -1 || sound->getCluster() == selectedCluster ) {
            sound->clusterIsHovered = true;
        } else {
            sound->clusterIsHovered = false;
        }
    }

//    string strDebug = "selectedClusters: ";
//    for ( auto cluster : selectedClusters ) {
//        strDebug += ofToString(cluster) + ",";
//    }
//    ofLog() << strDebug;
//    ofLog() << "selectedCluster: " << selectedCluster;
}

void Sounds::mouseDragged(ofVec2f p, int button) {
    if ( !originalPositionsToggle->getChecked() && button == 2 ) {
        Sound * hoveredSound = getHoveredSound();

        if ( hoveredSound != NULL ) {
            hoveredSound->position = p;
            updateSoundPosition(hoveredSound);
        }
    }
}


void Sounds::allSoundsSelectedOff() {
    for(int i = 0; i < sounds.size(); i++) {
        sounds[i]->selected = false;
    }
}
void Sounds::allSoundsHoveredOff(Sound * except) {
    for(int i = 0; i < sounds.size(); i++) {
        if ( except != NULL && sounds[i] == except ) {
            continue;
        }
        if ( sounds[i]->getHovered() ) {
            sounds[i]->setHovered(false);
        }
    }
}

void Sounds::playSoundAt(ofVec2f playerPosition) {
    Sound * nearestSound = getNearestSound(playerPosition);

    if ( nearestSound != NULL ) {
        playSound( nearestSound );
    }
}

Sound * Sounds::getNearestSound(ofVec2f position) {
    float minDistance = sounds[0]->getPosition().squareDistance( position );
    int minDistanceIndex = -1;

    float currentDistance = 0;

    for(int i = 1; i < sounds.size(); i++) {
        if ( sounds[i]->getHide() ) {
            continue;
        }

        currentDistance = sounds[i]->getPosition().squareDistance( position );

        if ( currentDistance > pow(sounds[i]->size * 3,2) ) {
            continue;
        }

        if ( currentDistance < minDistance ) {
            minDistance = currentDistance;
            minDistanceIndex = i;
        }
    }

    if ( minDistanceIndex != -1 ) {
        return sounds[minDistanceIndex];
    } else {
        return NULL;
    }
}

Sound * Sounds::getHoveredSound() {
    for(int i = 0; i < sounds.size(); i++) {
        if ( sounds[i]->hovered ) {
            return sounds[i];
        }
    }

    return NULL;
}

void Sounds::playSound( Sound * sound ) {
    vector<string> pathArr = ofSplitString(sound->getFileName(),"/");
    currentSound = pathArr[pathArr.size() - 1];
    // currentSound = sound->getFileName();

    //si no está cargado
    if(!sound->isLoaded()) {
        //te fijas si ya cargaste más que el limite
        if(nLoadedSounds >= MAX_LOADED_SOUNDS) {
            //empezas a ubicarlos del principio
            nLoadedSounds = 0;
        }
        //si el elemento actual no es NULL
        if(loadedSounds[nLoadedSounds] != NULL) {
            loadedSounds[nLoadedSounds]->unload();
            loadedSounds[nLoadedSounds] = NULL;
        }

        ofFile soundFile( sound->getFileName() );
        if ( soundFile.exists() ) {
            sound->load(sound->getFileName());
        } else {
            ofLog() << "Warning: Inexistent file " << sound->getFileName() << endl;
        }

        loadedSounds[nLoadedSounds] = sound;
        nLoadedSounds++;
    }
    sound->setVolume(getVolume());

    //esto me parece que habría que chequearlo desde más arriba
    if( !sound->isPlaying() || replaySound ) {
        sound->play();
        lastPlayedSound = sound;
    }

    setFilenameLabel( getCurrentSound() );
}

void Sounds::playSound(int id)
{
    playSound( getSoundById(id) );
}

void Sounds::getSpaceLimits(vector<string> soundsCoords) {
    ofVec3f max(-99999999, -99999999, -99999999);
    ofVec3f min(99999999, 99999999, 99999999);
    // spaceLimits = {ofVec2f(0,0),ofVec2f(1500,1500)};
    spaceLimits[0] = max;
    spaceLimits[1] = min;


    for(int i = 0; i< soundsCoords.size() - 1; i++) {
        if(soundsCoords[i] == "")continue;
        auto lineElements = ofSplitString(soundsCoords[i],",");

        ofVec3f position(ofToFloat(lineElements[TSV_FIELD_X]), ofToFloat(lineElements[TSV_FIELD_Y]), ofToFloat(lineElements[TSV_FIELD_Z]));

        spaceLimits[0].x = Utils::getMaxVal(position.x, spaceLimits[0].x);
        spaceLimits[0].y = Utils::getMaxVal(position.y, spaceLimits[0].y);
        spaceLimits[0].z = Utils::getMaxVal(position.z, spaceLimits[0].z);

        spaceLimits[1].x = Utils::getMinVal(position.x, spaceLimits[1].x);
        spaceLimits[1].y = Utils::getMinVal(position.y, spaceLimits[1].y);
        spaceLimits[1].z = Utils::getMinVal(position.z, spaceLimits[1].z);

    }

}

void Sounds::setupGui() {
    //Volume slider
    ofxDatGuiSlider * volumeSlider = gui->addSlider("Volume", 0, 1);
    volumeSlider->onSliderEvent(this, &Sounds::onSliderEvent);
    volumeSlider->setValue(0.5);
    volumeSlider->setStripeColor(ColorPaletteGenerator::stripeMainColor);

    //Toggle replay
    ofxDatGuiToggle *replayOnPlayToggle = gui->addToggle("Replay Sound");
    replayOnPlayToggle->onToggleEvent(this, &Sounds::onToggleEvent);
    replayOnPlayToggle->setChecked(replaySound);
    replayOnPlayToggle->setStripeColor( ColorPaletteGenerator::stripeMainColor );

    //Toggle playOnHover
    playOnHover = gui->addToggle("Play on hover");
    playOnHover->setChecked(true);

    playOnHover->setStripeColor(ColorPaletteGenerator::stripeMainColor);

    //Filename
    currentSoundLabel = gui->addLabel( "" );
    currentSoundLabel->setStripeColor(ColorPaletteGenerator::stripeMainColor);
    setFilenameLabel( "" );

    //Eps DBSCan
    gui->addBreak()->setBackgroundColor( ColorPaletteGenerator::stripeMainColor );
    folderClusters = gui->addFolder("Clusters", ColorPaletteGenerator::stripeMainColor);


    epsDbScan = folderClusters->addSlider("Clusters eps", 10, 50, 15);
    epsDbScan->onSliderEvent(this, &Sounds::onSliderEvent);
    epsDbScan->setPrecision(0);
    btnDbScan = folderClusters->addButton("Do clustering");
    btnDbScan->onButtonEvent(this, &Sounds::onDoClustering);

    originalPositionsToggle = folderClusters->addToggle("Use Original Positions");
    originalPositionsToggle->onToggleEvent(this, &Sounds::onToggleEvent);
    originalPositionsToggle->setChecked(true);
    originalPositionsToggle->setStripeColor( ColorPaletteGenerator::stripeMainColor);

    setupGuiClusters();

    //Original positions
    gui->addBreak()->setBackgroundColor(ofColor(0x2e2e2e));


}

void Sounds::setupGuiClusters()
{
//    ColorPaletteGenerator::setCantColors(clusterIds.size());
    folderClusters->setLabel("Clusters (" + ofToString(clusterIds.size()) + ")");

    //Toggle Clusters
//    ofLog() << "Clusters: " << clusterIds.size();
//    int nClusters = clusterIds.size();//ColorPaletteGenerator::colors.size();

//    if ( matrixClusters != nullptr ) {
//        delete matrixClusters;
//        matrixClusters = nullptr;
//    }
//    matrixClusters = gui->addMatrix("Clusters", nClusters,true);
//    matrixClusters->onMatrixEvent(this,&Sounds::onMatrixEvent);
//    matrixClusters->setLabelColor(ofColor::white);
//    matrixClusters->setStripeColor( ofColor(0,0,0) );

//    ofxDatGuiTheme * theme = new ofxDatGuiThemeCharcoal();
//    for ( auto i = 0 ; i < nClusters ; i++ ) {
//        ofxDatGuiMatrixButton* cluster = matrixClusters->getChildAt(i);
//        ofColor color(ColorPaletteGenerator::getColor(i));
//        theme->color.matrix.normal.button = color;
//        cluster->setTheme(theme);
//    }
}
void Sounds::onSliderEvent(ofxDatGuiSliderEvent e) {
    if (e.target->getLabel() == "Volume") {
        volumen = e.target->getValue();
    }
}
void Sounds::onToggleEvent(ofxDatGuiToggleEvent e) {
    if (e.target->getLabel() == "Replay Sound") {
        replaySound = e.target->getChecked();
    }
    if (e.target->getLabel() == "Use Original Positions") {
        setUseOriginalPositions(e.target->getChecked());
    }
}

void Sounds::setUseOriginalPositions(bool yes) {
    for(int i = 0; i<soundPositions.size(); i++) {
        if (yes) {
            soundPositions[i].sound->useOriginalPosition();
        } else {
            soundPositions[i].sound->position = soundPositions[i].position;
        }
    }
}

void Sounds::onMatrixEvent(ofxDatGuiMatrixEvent e) {
    for(int i = 0; i<sounds.size(); i++) {
        if(sounds[i]->getCluster() == e.child) {
            sounds[i]->setHide(e.enabled);
        }

    }
}

void Sounds::onDoClustering(ofxDatGuiButtonEvent e)
{
    DBScan dbscan( epsDbScan->getValue() , 5, getSoundsAsPoints());
    dbscan.run();
    setDBScanClusters(dbscan.points);
    findConvexHulls();
    generateBackground();
}

Sound * Sounds::getSoundByFilename(string filename) {
    for ( int i = 0 ; i < sounds.size() ; i++ ) {
        if ( sounds[i]->getFileName() == filename ) {
            return sounds[i];
        }
    }
    return NULL;
}
Sound * Sounds::getSoundById(int id) {
    for ( int i = 0 ; i < sounds.size() ; i++ ) {
        if ( sounds[i]->id == id ) {
            return sounds[i];
        }
    }
    return NULL;
}

vector<Sound *> Sounds::getSounds()
{
    return sounds;
}

vector<ofPoint> Sounds::getSoundsAsPoints()
{
    vector<ofPoint> soundsAsPoints;

    for ( int i = 0 ; i < sounds.size() ; i++ ) {
        soundsAsPoints.push_back( sounds[i]->getPosition() );
    }

    return soundsAsPoints;
}

void Sounds::setDBScanClusters(vector<dbscanPoint> points)
{
    clusterIds.clear();

    for ( int i = 0 ; i < sounds.size() ; i++ ) {
        sounds[i]->setCluster( points[i].cluster );

        if ( points[i].cluster >= 0 ) {
            clusterIds.insert( points[i].cluster );
        }
    }

    setupGuiClusters();
}


float Sounds::getVolume() {
    return volumen;
}
string Sounds::getCurrentSound() {
    return currentSound;
}

void Sounds::setFilenameLabel(string fileName)
{
    if ( currentSoundLabel != nullptr ) {
        currentSoundLabel->setLabel("Filename: " + fileName);
    }
}

Json::Value Sounds::save() {
    Json::Value root = Json::Value( Json::objectValue );
    Json::Value overridedPositions = Json::Value( Json::arrayValue );

    for ( int i = 0 ; i < soundPositions.size() ; i++ ) {
        Json::Value soundPosition = Json::Value( Json::objectValue );
        soundPosition["id"] = soundPositions[i].sound->id;
        soundPosition["position"] = Json::Value( Json::arrayValue );
        soundPosition["position"].append( soundPositions[i].position.x );
        soundPosition["position"].append( soundPositions[i].position.y );

        overridedPositions.append(soundPosition);
    }

    root["overridedPositions"] = overridedPositions;
    root["DBScan_EPS"] = epsDbScan->getValue();
    return root;
}

void Sounds::load( Json::Value jsonData ) {
    Json::Value overridedPositions = jsonData["overridedPositions"];
    for ( int i = 0 ; i < overridedPositions.size() ; i++ ) {
        // Sound * sound = getSoundById( overridedPositions[i]["id"] )
        soundPosition newSoundPosition;
        newSoundPosition.sound = getSoundById( overridedPositions[i]["id"].asInt() );

        if ( newSoundPosition.sound == NULL ) {
            ofLogWarning("Sound ID not found when loading session");
            continue;
        }

        newSoundPosition.position.x = overridedPositions[i]["position"][0].asInt();
        newSoundPosition.position.y = overridedPositions[i]["position"][1].asInt();

        soundPositions.push_back(newSoundPosition);
    }

    if ( jsonData["DBScan_EPS"].isInt() ) {
        epsDbScan->setValue( jsonData["DBScan_EPS"].asInt() );
    }

    onDoClustering(NULL);
}
