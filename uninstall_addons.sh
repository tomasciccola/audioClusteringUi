#!/bin/sh
if [ -z "${OF_ROOT}" ]; then
    echo "Please define OF_ROOT enviroment variable.\n\n export OF_ROOT=<path/to/of>\n "
    exit
fi

PROJECT_DIR=$(dirname "$0")

rm -r $PROJECT_DIR/bin/data/assets

rm -r $OF_ROOT/addons/ofxMidi
rm -r $OF_ROOT/addons/ofxSimpleTimer
rm -r $OF_ROOT/addons/ofxJSON
rm -r $OF_ROOT/addons/ofxConvexHull
rm -r $OF_ROOT/addons/ofxTweener

rm -r $OF_ROOT/addons/ofxDatGui
rm -r $PROJECT_DIR/bin/data/ofxbraitsch

#rm -r $OF_ROOT/addons/ofxInfiniteCanvas

echo "DONE"
