#!/bin/sh
if [ -z "${OF_ROOT}" ]; then
    echo "Please define OF_ROOT enviroment variable.\n\n export OF_ROOT=<path/to/of>\n "
    exit
fi

PROJECT_DIR=$(dirname "$0")
#echo "$PROJECT_DIR"

mkdir -p $PROJECT_DIR/bin/data 
ln -s ../../assets/ $PROJECT_DIR/bin/data/assets

git clone https://github.com/danomatika/ofxMidi $OF_ROOT/addons/ofxMidi
git clone https://github.com/HeliosInteractive/ofxSimpleTimer $OF_ROOT/addons/ofxSimpleTimer
git clone https://github.com/jeffcrouse/ofxJSON $OF_ROOT/addons/ofxJSON

git clone https://github.com/genekogan/ofxConvexHull $OF_ROOT/addons/ofxConvexHull
printf "\nAdding include line to ofxConvexHull\n\n"
sed -i '12 i\#include "ofMain.h"' $OF_ROOT/addons/ofxConvexHull/src/ofxConvexHull.h

git clone https://github.com/larsberg/ofxTweener $OF_ROOT/addons/ofxTweener

git clone https://github.com/braitsch/ofxDatGui $OF_ROOT/addons/ofxDatGui
printf "\nAdding include line to ofxSmartFont\n\n"
sed -i '26 i\#include "ofMain.h"' $OF_ROOT/addons/ofxDatGui/src/libs/ofxSmartFont/ofxSmartFont.h
cp -R $OF_ROOT/addons/ofxDatGui/ofxbraitsch/ $PROJECT_DIR/bin/data 

# git clone https://github.com/roymacdonald/ofxInfiniteCanvas $OF_ROOT/addons/ofxInfiniteCanvas
#(cd $OF_ROOT/addons/ofxInfiniteCanvas; git checkout 2087e4fb96fbf87e41d3dc890e7491cf8b4d380f)
echo "DONE"
